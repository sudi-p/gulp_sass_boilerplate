var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minify = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename')
    watch = require('gulp-watch');

gulp.task('sass',function(){
    gulp.src('./src/scss/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(minify())
    .pipe(rename('style.css'))
    .pipe(gulp.dest('./dist/css/'))
})

gulp.task('watch', function(){
    gulp.watch('./src/scss/*/*.scss', [sass])
})

gulp.task('default',['sass', 'watch'])
